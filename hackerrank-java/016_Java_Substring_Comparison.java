

    public static String getSmallestAndLargest(String s, int k) {
        String smallest = s.substring(0,k);
        String largest = "";


        for(int i=0; i<s.length()-k+1; i++){
            String temp = s.substring(i,i+k);
            if (temp.compareTo(smallest) < 0) {
                smallest = temp;
            }
            if (temp.compareTo(largest) > 0) {
                largest = temp;
            }
        }
        return smallest + "\n" + largest;
    }



