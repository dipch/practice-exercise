select s.name, g.grade, s.marks
from students s
inner join grades g
on s.marks between min_mark and max_mark
and g.grade>7
order by g.grade desc, s.name asc, s.marks asc

select 'NULL', g.grade, s.marks
from students s
inner join grades g
on s.marks between min_mark and max_mark
and g.grade<8
order by g.grade desc, s.name asc, s.marks asc
